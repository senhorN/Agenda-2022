﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/* trabalhando com arquivos */
using System.IO; //input e Output de dados
                 //

//movimento o form pelo painel
using System.Runtime.InteropServices;

namespace Agenda2022
{
    public partial class Form1 : Form
    {
        //usado para mover a janela pelo painel
        #region
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
            int msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion



        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
           /*
            if ( MessageBox.Show("Tem certeza que quer sair?", "informação",MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    this.Close();
                }
           */
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Deseja relamente fechar o app?", "informação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            if ((txtNome.Text != "") && (txtFone.Text != "") && (cmbSexo.Text != ""))
            {
                if (MessageBox.Show("deseja inserir a lista",
                    "Informação",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.Yes)
                
                    lstAgenda.Items.Add(txtNome.Text + "|" + txtFone.Text + "|" + cmbSexo.Text);
                    
                    /*btnNovo.PerformClick();*/

            }
            else
            {
                MessageBox.Show("Preencha todos os dados",
                    "informação",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                txtNome.Focus();

            }
            

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja limpar todos os campos ?",
                "informação",
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                txtNome.Clear();
                txtFone.Text = "";
                cmbSexo.Text = "";
                txtNome.Focus();
                
                

            }
            

            
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            if(lstAgenda.Items.Count > 0)
            {
                if (lstAgenda.SelectedIndex > -1)
                {
                    string[] campos = lstAgenda.SelectedItem.ToString().Split('|');
                    txtNome.Text = campos[0]; /*Linha 1 posição 0 */
                    txtFone.Text = campos[1]; /*Linha 1 posição 1 */
                    cmbSexo.Text = campos[2]; /*Linha 1 posição 2 */
                }
            }
            else
            {
                MessageBox.Show(
                "Não há registros gravados!",
                "informação", 
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            }
            
        }

        private void btnLimparAgenda_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Deseja limpar todos os campos ?",
                "informação",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            { 
                lstAgenda.Items.Clear();
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            if (lstAgenda.SelectedIndex > -1)
            {
                if (MessageBox.Show("Deseja excluir o item selcionado?",
                    "informação",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    lstAgenda.Items.RemoveAt(lstAgenda.SelectedIndex);
                    lstAgenda.SelectedIndex = -1;
                }
                else
                {
                    MessageBox.Show("Operação cancelada!",
                    "informação",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                    lstAgenda.SelectedIndex = -1;

                }
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            FileInfo salvararquivo = new FileInfo(@"./agenda.txt");
            if (lstAgenda.Items.Count > 0)
            {
                if (!salvararquivo.Exists)
                {
                    using (StreamWriter sa = salvararquivo.CreateText())
                    {
                        for (int i = 0; i < lstAgenda.Items.Count; i++)
                        {
                            sa.WriteLine(lstAgenda.Items[i].ToString());
                        }
                    }
                    MessageBox.Show("Backup atualizado com sucesso!");
                }
                else
                {
                    if (MessageBox.Show("Deseja substituir o backup!",
                        "informação", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                    }
                }
            }
            else
            {
                MessageBox.Show("A lista esta vazia!",
                    "informação", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void lblAgenda_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Opacity = 0.7; //diminui a opacidade quando o formulario é selecionado
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            this.Opacity = 1; //volta a opacidade para o estado normal
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            FileInfo AbrirArquivo = new FileInfo(@"./agenda.txt");
                
            if(AbrirArquivo.Exists)
            {   
                using (StreamReader aa = AbrirArquivo.OpenText())
                {   //limpa a listbox
                    lstAgenda.Items.Clear();
                    //variável que irá receber as linhas do arquivo 
                    string linha = "";
                    //enquanto usado para ler todas linhas do arq.
                    //enquanto a leitura da linha for diferente de
                    //null a leitura continua 
                    while((linha = aa.ReadLine()) != null) // ! + = = diferente
                    {   
                        //adiciona a linha na listbox
                        lstAgenda.Items.Add(linha);
                    }
                }
                
            }
            else
            {
                MessageBox.Show(
                    "O arquivo de backup não existe!",
                    "informação",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtFone.Focus();
            }
        }

        private void txtFone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                cmbSexo.Focus();
            }
        }

        private void cmbSexo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                btnAdicionar.PerformClick();
            }
        }
    }
}
